import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import axios from '../../axios-pages';

class Page extends Component {
  state = {
    title: '',
    content: ''
  };

  getDataToState = () => {
    const thisPage = this.props.match.params.page;
    axios.get('/' + thisPage + '.json').then(response => {
      this.setState({title: response.data.title, content: response.data.content});
    })
  };

  componentDidMount() {
    this.getDataToState();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.page !== this.props.match.params.page) {
      this.getDataToState();
    }
  }

  render() {
    return (
      <div className="Page">
        <Container>
          <h2>{this.state.title}</h2>
          <p>{this.state.content}</p>
        </Container>
      </div>
    );
  }
}

export default Page;
