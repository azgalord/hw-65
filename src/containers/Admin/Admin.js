import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import Form from "../../components/Form/Form";
import axios from '../../axios-pages';

class Admin extends Component {
  state = {
    title: '',
    content: '',
    select: 'home'
  };

  componentDidMount() {
    axios.get('/home.json').then(response => {
      this.setState({title: response.data.title, content: response.data.content});
    })
  }

  changeHandler = (event) => {
    const thisText = event.target.name;
    const value = event.target.value;

    this.setState({[thisText] : value});

    if (thisText === 'select') {
      axios.get('/' + value + '.json').then(response => {
        this.setState({title: response.data.title, content: response.data.content});
      })
    }
  };

  saveChanges = (event) => {
    event.preventDefault();

    const object = {
      title: this.state.title,
      content: this.state.content
    };

    axios.put('/' + this.state.select + '.json', object).finally(() => {
      console.log(this.props.history.replace('/pages/' + this.state.select));
    })
  };

  render() {
    return (
      <div className="Admin">
        <Container>
          <h2>Edit pages</h2>
          <Form
            change={event => this.changeHandler(event)}
            title={this.state.title}
            content={this.state.content}
            save={(event) => this.saveChanges(event)}
          />
        </Container>
      </div>
    );
  }
}

export default Admin;
