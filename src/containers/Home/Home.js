import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import axios from '../../axios-pages';

class Home extends Component {
  state = {
    title: '',
    content: ''
  };

  componentDidMount() {
    axios.get('/home.json').then(response => {
      this.setState({title: response.data.title, content: response.data.content});
    })
  }

  render() {
    return (
      <div className="Home">
        <Container>
          <h2>{this.state.title}</h2>
          <p>{this.state.content}</p>
        </Container>
      </div>
    );
  }
}

export default Home;
