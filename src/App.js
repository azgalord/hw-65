import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Page from "./containers/Page/Page";
import Home from './containers/Home/Home';
import Admin from "./containers/Admin/Admin";

import './App.css';

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/pages/admin" exact component={Admin}/>
          <Route path="/pages/:page" exact component={Page}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
