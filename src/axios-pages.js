import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://hw-65-azamat.firebaseio.com/'
});

export default instance;
