import React from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";
import Logo from "../Logo/Logo";

import './Header.css';
import Container from "../Container/Container";

const Header = props => {
  return (
    <div className="Header">
      <Container>
        <Logo/>
        <Toolbar/>
      </Container>
    </div>
  );
};

export default Header;
