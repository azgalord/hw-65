import React from 'react';

import './Form.css';

const Form = props => {
  return (
    <div className="Form">
      <form>
        <label htmlFor="select">Select page</label>
        <select onChange={props.change} name="select" id="select">
          <option>home</option>
          <option>about</option>
          <option>contacts</option>
          <option>division</option>
          <option>features</option>
          <option>projects</option>
        </select>
        <label htmlFor="title">Title</label>
        <input value={props.title} onChange={props.change} type="text" name="title" id="title"/>
        <label htmlFor="content">Content</label>
        <textarea value={props.content} onChange={props.change} name="content" id="content" cols="30" rows="10"/>
        <button type="submit" onClick={props.save}>Save</button>
      </form>
    </div>
  );
};

export default Form;
