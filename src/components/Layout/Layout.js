import React, {Fragment} from 'react';
import Header from "../Header/Header";

const Layout = ({children}) => {
  return (
    <Fragment>
      <Header/>
      <main className="LayoutContent">
        {children}
      </main>
      <div></div>
    </Fragment>
  );
};

export default Layout;
