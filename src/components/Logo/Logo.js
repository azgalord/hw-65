import React from 'react';
import LogoImg from '../../assets/img/logo.jpeg';

import './Logo.css';

const Logo = () => {
  return (
    <div className="Logo">
      <img src={LogoImg} alt=""/>
    </div>
  );
};

export default Logo;
