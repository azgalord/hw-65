import React from 'react';
import NavItem from "../NavItem/NavItem";

import './Toolbar.css';

const Toolbar = () => (
  <nav className="Toolbar">
    <ul>
      <NavItem to="/" exact>Home</NavItem>
      <NavItem to="/pages/about" exact>About</NavItem>
      <NavItem to="/pages/contacts" exact>Contacts</NavItem>
      <NavItem to="/pages/division" exact>Division</NavItem>
      <NavItem to="/pages/features" exact>Features</NavItem>
      <NavItem to="/pages/projects" exact>Projects</NavItem>
      <NavItem to="/pages/admin" exact>Admin</NavItem>
    </ul>
  </nav>
);

export default Toolbar;
