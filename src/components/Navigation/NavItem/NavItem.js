import React from 'react';
import {NavLink} from 'react-router-dom';

import './NavItem.css';

const NavItem = ({to, exact, children}) => (
  <li className="NavItem">
    <NavLink to={to} exact={exact}>{children}</NavLink>
  </li>
);

export default NavItem;
